package com.capital.salt.exception;

import org.springframework.http.HttpStatus;

import lombok.Data;

@Data
public class ExceptionResponse {

	private String errorMessage;
	private String requestedURI;
	private HttpStatus httpStatus;
	private int httpStatusCode;
}
