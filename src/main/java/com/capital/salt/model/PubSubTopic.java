package com.capital.salt.model;

import java.io.Serializable;
import java.sql.Timestamp;

import lombok.Data;

@Data
public class PubSubTopic implements Serializable {

	private static final long serialVersionUID = 165098626153402592L;
	private String id;
	private String topicName;
	private String topicDescription;
	private String topicCreatedBy;
	private Timestamp topicCreatedTimeStamp;
}
